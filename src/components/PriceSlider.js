import React from 'react';
import { Box, Stack, Text, Slider, HStack, Image } from "native-base";
import iconSlider from '../../assets/images/icon-slider.png'
import { StyleSheet } from 'react-native';

const PriceSlider = ({ title, setOnChangeValue, actualPrice }) => {

    return <Box alignItems="center" w="100%">
        <Stack space={4} alignItems="center" w="75%" maxW="300" mt="2">
            <Slider size="lg" defaultValue={50.00} step={0.01} onChange={v => {
                setOnChangeValue(v);
            }}>
                <Slider.Track bg="#ECF0FB">
                    <Slider.FilledTrack bgColor="#A4F3EB" />
                </Slider.Track>
                <Slider.Thumb borderWidth="0" bg="transparent" style={{ ...styles.shadow }}>
                    <Box w="30px" h="30px" bg="#10D8C4" justifyContent="center" alignItems="center" borderRadius="full">
                        <Image source={iconSlider} w="18px" h="10px" />
                    </Box>
                </Slider.Thumb>
            </Slider>
            <HStack alignItems="center" mt="4">
                <Text textAlign="center" fontSize="2xl" color="#293356" style={{ ...styles.font.header }}>${actualPrice} </Text>
                <Text textAlign="center" color="#848EAD" style={{ ...styles.font.body }}> / {title}</Text>
            </HStack>
        </Stack>
    </Box>;
};

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#A4F3EB",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 1,
        shadowRadius: 16,

        elevation: 35,
    },
    font: {
        header: { fontFamily: "Manrope-ExtraBold" },
        body: { fontFamily: "Manrope-SemiBold" },
    }
});

export default PriceSlider;
