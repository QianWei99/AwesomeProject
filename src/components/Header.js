import React from 'react';
import { Center, Text, Image } from "native-base";
import circleImage from '../../assets/images/pattern-circles.png'
import { StyleSheet } from 'react-native';


const Header = () => {

    return (
        <Center>
            <Image source={circleImage} w="140px" h="140px" top="-390px" position="absolute" />
            <Text fontSize="lg" mb="3" top="-360px" position="absolute" color="#293356" style={{...styles.font.header}}>Simple, traffic-based pricing</Text>
            <Text color="#848EAD" position="absolute" top="-320px" style={{...styles.font.body}}>Sign-up for our 30-day trial.</Text>
            <Text mb="3" color="#848EAD" position="absolute" top="-295px" style={{...styles.font.body}}>No credit card required.</Text>
        </Center>
    );
};

const styles = StyleSheet.create({
    font: {
        header: {fontFamily:"Manrope-ExtraBold"},
        body: {fontFamily:"Manrope-SemiBold"},
    }
});


export default Header;
