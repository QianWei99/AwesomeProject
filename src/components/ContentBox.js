import React, { useEffect, useState } from 'react';
import { Box, Center, Text, HStack, Switch, Divider, CheckIcon, Button } from "native-base";
import PriceSlider from '../components/PriceSlider'
import { StyleSheet } from 'react-native';

const ContentBox = () => {
    const [title, setTitle] = useState('month')
    const [change, setChange] = useState(false)
    const [onChangeValue, setOnChangeValue] = useState(50.00);
    const [actualPrice, setActualPrice] = useState()
    const [monthValue, setMonthValue] = useState(50.00)

    const yearValue = onChangeValue * 12 * 0.75

    function onChange() {
        setChange(!change)
    }

    useEffect(() => {
        if (change === true) {
            setTitle('year')
            setActualPrice(Math.floor(yearValue * 100) / 100)
        }
        else {
            setTitle('month')
            setActualPrice(monthValue)
        }
    }, [change, monthValue, yearValue])

    useEffect(() => {
        setMonthValue(onChangeValue)
    }, [onChangeValue])

    return (
        <Center>
            <Box p="4" mt="10" alignItems="center" bgColor="#FFFFFF" w="80%" borderRadius="8" position="absolute" top="-250px" style={{ ...styles.shadow }}>
                <Text my="5" color="#848EAD" letterSpacing="2xl" fontSize="xs" style={{...styles.font.header}}>100K PAGEVIEWS</Text>
                <PriceSlider title={title} setOnChangeValue={setOnChangeValue} actualPrice={actualPrice} onChangeValue={onChangeValue} />
                <HStack alignItems="center" space={2} mt="5">
                    <Text fontSize="xs" color="#848EAD" style={{...styles.font.body}}>Monthly Billing</Text>
                    <Switch size="sm" offTrackColor="#CFD8EF" onTrackColor="#10D8C4" onChange={onChange} isChecked={change} />
                    <Text fontSize="xs" color="#848EAD" style={{...styles.font.body}}>Yearly Billing</Text>
                    <Box borderRadius="15" bgColor="#FEEDE8" justifyContent="center" alignItems="center" w="50px"><Text color="#FF8D68" fontSize="xs">-25%</Text></Box>
                </HStack>
                <Divider my="5" bgColor="#F1F5FE" />
                <HStack space={2} mb="2">
                    <CheckIcon size="4" mt="0.5" color="emerald.500" />
                    <Text fontSize="xs" color="#848EAD" style={{...styles.font.body}}>Unlimited websites</Text>
                </HStack>
                <HStack space={2} mb="2">
                    <CheckIcon size="4" mt="0.5" color="emerald.500" />
                    <Text fontSize="xs" color="#848EAD" style={{...styles.font.body}}>100% data ownership</Text>
                </HStack>
                <HStack space={2} mb="6">
                    <CheckIcon size="4" mt="0.5" color="emerald.500" />
                    <Text fontSize="xs" color="#848EAD" style={{...styles.font.body}}>Email reports</Text>
                </HStack>
                <Button bgColor="#293356" borderRadius="20" w="170px"><Text fontSize="xs" color="#BECDFF">Start my trial</Text></Button>
            </Box>
        </Center>
    );
};

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#848EAD",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.3,
        shadowRadius: 15,
        elevation: 5,
    },
    font: {
        header: {fontFamily:"Manrope-ExtraBold"},
        body: {fontFamily:"Manrope-SemiBold"},
    }
});


export default ContentBox;
