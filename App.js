/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NativeBaseProvider, Box, Button } from "native-base";
import { SafeAreaView } from 'react-native-safe-area-context';
import Header from './src/components/Header';
import ContentBox from './src/components/ContentBox';
import Svg, { Path } from 'react-native-svg';

const App = () => {

  return (
    <NativeBaseProvider>
      <SafeAreaView>
        <Box position="relative" top="-60px">
          <Svg xmlns="http://www.w3.org/2000/svg" width="1440" height="449">
            <Path fill="#F1F5FE" fill-rule="evenodd" d="M0 0h1440v449H191.5C85.737 449 0 363.263 0 257.5V0z" />
          </Svg>
        </Box>
        <Header />
        <ContentBox />
      </SafeAreaView>
    </NativeBaseProvider>
  );
};

export default App;
